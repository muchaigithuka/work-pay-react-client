
import React, {Component} from 'react';
import Contacts from './components/contacts';

class App extends Component {
    render() {
        return (
            <Contacts contacts={this.state.contacts} />
        )
    }

    state = {
        contacts: []
    };
    componentDidMount() {
        var targetUrl = 'http://localhost:8003/api/core/products/call/v1'
        fetch(targetUrl).then(res => res.json())
            .then((data) => {
                this.setState({ contacts: data })
            })
            .catch(console.log)
    }
}
export default App;