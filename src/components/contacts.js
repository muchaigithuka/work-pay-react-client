import React from 'react'

    const Contacts = ({ contacts }) => {
      return (
        <div class="container">
        <div>

          <center><h1>WorkPay Product Management System</h1></center>
          
          <div class="row">
          {contacts.map((contact) => (
              <div class="col-md-3">
          
            <div class="card">
            <div class="card-header">
                Product Id : #{contact.id}
               </div>
              <div class="card-body">
                <h5 class="card-title"></h5>
                <h6 class="card-subtitle mb-2 text-muted">{contact.sku}</h6>
                <p class="card-text">{contact.description}</p>
                <a href="#" class="btn btn-primary">KES {contact.price} .00</a>
              </div>
            </div> 
          </div>
          ))}
          </div>
          
        </div>
        </div>
      )
    };

    export default Contacts